from django.http.response import JsonResponse
from rest_framework import status
from rest_framework import response
from rest_framework import permissions
from rest_framework.response import Response
from rest_framework.views import APIView
from .serializer import UserSerializer, AuthenticatedUser 
from rest_framework.authtoken.views import ObtainAuthToken
import jwt 
import datetime
from authentication.settings import SECRET_KEY #Encriptación del token
from django.contrib.auth.models import User
from rest_framework.permissions import IsAuthenticated

class UserAPI(APIView):
    def post(self, request): #Agregar un nuevo usuario
        serializer=UserSerializer(data=request.data)
        if serializer.is_valid():
            user=serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        else:
            return Response(serializer.data, status=status.HTTP_400_BAD_REQUEST)

    def get(self, request, *args, **kwargs): #Listar usuarios
        #print(kwargs) #Devuelve el parametro request
        if kwargs.get('id') is None: #Toda la informacion de los usuarios
            users=User.objects.all() 
            user_serializers=UserSerializer(users,many=True) #Sale error porque recibe todos los usuarios many=true
            return Response(user_serializers.data,status=status.HTTP_200_OK)
        else:  #Información de un usuario especifico
            user=User.objects.get(id=kwargs.get('id'))
            user_serializers=UserSerializer(user)
            return Response(user_serializers.data,status=status.HTTP_200_OK)
    def put(self, request, *args, **kwargs): #Editar un usuario Id y request
        if kwargs.get('id') is None:
            return Response({'error':'Debe proporcionar id de usuario a editar'},status=status.HTTP_400_BAD_REQUEST)
        user=User.objects.get(id=kwargs.get('id'))
        user_serializers=AuthenticatedUser(user,data=request.data) #Request recibe una nueva información
        if user_serializers.is_valid():
            user_serializers.save()
            return Response(user_serializers.data,status=status.HTTP_200_OK) #Información actualizada
        else:
            return Response(user_serializers.errors,status=status.HTTP_400_BAD_REQUEST)
    
    def delete(self, request, *args, **kwargs): #Eliminar el usuario
        if kwargs.get('id') is None:
            return Response({'error':'Debe proporcionar id de usuario a eliminar'},status=status.HTTP_400_BAD_REQUEST)
        user=User.objects.get(id=kwargs.get('id'))
        user.delete()
        return Response({'message':'El usuario ha sido eliminado'})
        

class Login(ObtainAuthToken):
    def post(self,request):
        login_serializer=self.serializer_class(data=request.data,context={'request':request})
        if login_serializer.is_valid():
            user=login_serializer.validated_data['user']
            if user.is_active:
                user_serializer=AuthenticatedUser(user)
                date=datetime.datetime.now()
                token=jwt.encode(payload={
                    'exp':date+datetime.timedelta(hours=2), #tiempo de duración del token
                    'user':user_serializer.data
                },key=SECRET_KEY)
                return Response({'menssage':'Sesión iniciada',
                                'token':token,
                                'user':user_serializer.data
                },status=status.HTTP_201_CREATED)
            else: 
                return Response({'error':'Este usuario no puede iniciar'},status=status.HTTP_401_UNAUTHORIZED)
        else:
            return Response({'error':'Credenciales incorrectos'},status=status.HTTP_400_BAD_REQUEST)

class AuthView(APIView):
    user: User
    def dispatch(self, request, *args, **kwargs): #Lo primero que se ejecuta al momento de hacer una petición
        try:
            if not request.headers.get('Authorization'):
                raise Exception('No hay token')
            token= request.headers.get('Authorization').split(' ')[1]
            # "Bearer dfgkjldgjwi32ptrlkgjd3" ----> Obtener solo lo que esta despues del Bearer
            decoded=jwt.decode(token,key=SECRET_KEY,algorithms=["HS256"])
            #print(decoded)
            self.user= User.objects.get(id=decoded.get('user_id'))
            #print(self.user)
            if self.user.is_active:
                return super().dispatch(request, *args, **kwargs)
            else:
                raise Exception('El usuario no está activo')
        except Exception as e:
            return JsonResponse({'error':e.__str__()},status=status.HTTP_401_UNAUTHORIZED)

class VerifyTokenManually(AuthView): #Como hereda de AuthView se verifica primero si el token es valido
    def get(self,request):
        return Response({'status':'ok'},status=status.HTTP_200_OK)
        

class VerifyToken(APIView):
    permissions_classes=[IsAuthenticated]
    def get(self,request):
        return Response({'message':'Autenticado correctamente'},status=status.HTTP_200_OK)
